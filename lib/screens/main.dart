import 'package:flutter/material.dart';
import 'package:jobfindrr/screens/applications.dart';
import 'package:jobfindrr/screens/home.dart';
import 'package:jobfindrr/screens/profile.dart';
import 'package:jobfindrr/screens/unknown.dart';

class Main extends StatefulWidget {
  const Main({Key? key}) : super(key: key);

  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  static const List<Widget> _screens = <Widget>[
    Home(),
    Applications(),
    Unknown(),
    Profile(),
  ];

  static const List<BottomNavigationBarItem> _navigationBarItems =
      <BottomNavigationBarItem>[
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: 'Home',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.receipt_rounded),
      label: 'Applications',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.device_unknown),
      label: '?',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.person),
      label: 'Profile',
    ),
  ];

  int _selectedIndex = 0;

  String _screenTitle = _screens.elementAt(0).toString();

  void _onItemTap(int index) => setState(() => {
        _selectedIndex = index,
        _screenTitle = _screens.elementAt(index).toString(),
      });

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text(_screenTitle),
          actions: [
            IconButton(
              onPressed: () => {},
              icon: Icon(Icons.settings),
            ),
          ],
        ),
        body: Container(
          child: _screens.elementAt(_selectedIndex),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          onPressed: () => {},
          tooltip: 'Search',
          child: Icon(Icons.search),
        ),
        bottomNavigationBar: BottomNavigationBar(
          elevation: 0.0,
          onTap: _onItemTap,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          currentIndex: _selectedIndex,
          items: _navigationBarItems,
        ),
      );
}
