import 'package:flutter/material.dart';
import 'package:jobfindrr/screens/main.dart';

class JobfindrrApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Main(),
      );
}
